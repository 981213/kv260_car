#!/usr/bin/env python3
import rosbag
from cv_bridge import CvBridge
import cv2
import csv
import sys

bridge = CvBridge()

with rosbag.Bag(sys.argv[2], 'r') as bag, open(sys.argv[1], 'a', newline='') as csvfile:
        speed = 0
        angle = 0
        img_counter = 0
        writer = csv.writer(csvfile)
        for topic, msg, t in bag.read_messages(topics=['/usb_cam/image_raw', '/ackermann_cmd']):
                if topic == '/ackermann_cmd':
                        print("command: speed {} angle {}".format(msg.drive.speed,msg.drive.steering_angle))
                        speed = msg.drive.speed
                        angle = msg.drive.steering_angle
                elif speed > 0: # usb_cam/image_raw
                        cv_image = bridge.imgmsg_to_cv2(msg, desired_encoding='bgr8')
                        img_roi = cv_image[240:480, 0:640]
                        img_name = '{}/{}.png'.format(sys.argv[3], img_counter)
                        cv2.imwrite(img_name, img_roi)
                        writer.writerow([img_name, angle])
                        img_counter += 1
                        #cv2.imshow("driving_img", img_roi)
                        #cv2.waitKey(0) 
                        #cv2.destroyAllWindows()
                #print(topic)
                #print(t)
