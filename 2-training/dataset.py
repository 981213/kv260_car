import os
import csv
import torch
from torchvision.io import read_image
from torch.utils.data import Dataset

class LaneDataset(Dataset):
    def __init__(self, label_path, transform=None, target_transform=None):
        self.transform = transform
        self.target_transform = target_transform
        self.labels = []
        with open(label_path, 'r', newline='') as csvfile:
            csv_reader = csv.reader(csvfile)
            for row in csv_reader:
                self.labels.append(row)

    def __len__(self):
        return len(self.labels)

    def __getitem__(self, idx):
        img_path = os.path.join(self.labels[idx][0])
        image = read_image(img_path)
        label = float(self.labels[idx][1])
        label = torch.tensor([label])
        if self.transform:
            image = self.transform(image)
        if self.target_transform:
            label = self.target_transform(label)
        return image, label