import pytorch_nndct

import torch
from torch.utils.data import DataLoader
from torch import nn
import torchvision.transforms as transforms
import torch.optim as optim
from dataset import LaneDataset
from model_reg import Net

from pytorch_nndct.apis import torch_quantizer, dump_xmodel
import sys

quant_mode = sys.argv[1]
if quant_mode == 'calib':
  batch_size = 100
elif quant_mode == 'test':
  batch_size = 1

transform = transforms.Compose([
    transforms.ConvertImageDtype(torch.float),
    transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))])

test_dataset = LaneDataset('dataset_rgb/subtest.csv', transform=transform)
testloader = DataLoader(test_dataset, batch_size=batch_size)

net = Net()

net.load_state_dict(torch.load('state_dict_rgb.pth'))
#net.eval()
device = 'cuda' if torch.cuda.is_available() else 'cpu'
print(f'Using {device} device')
device = torch.device(device)

input = torch.randn([batch_size, 3, 240, 640])
quantizer = torch_quantizer(quant_mode, net, (input), device=device)
quant_model = quantizer.quant_model

quant_model.eval()
quant_model.to(device)


def evaluate(model, val_loader, loss_fn):
  correct = 0.0
  total = 0
  for data in val_loader:
      inputs, labels = data
      inputs = inputs.to(device)
      labels = labels.to(device)
      # calculate outputs by running images through the network
      outputs = model(inputs)
      total += labels.size(0)
      correct += (labels - outputs).abs().sum()
      #print("Group ",(labels - outputs))
  print(f'avg err {correct / total}')

loss_fn = nn.MSELoss().to(device)

evaluate(quant_model, testloader, loss_fn)

if quant_mode == 'calib':
  quantizer.fast_finetune(evaluate, (quant_model, testloader, loss_fn))
elif quant_mode == 'test':
  quantizer.load_ft_param()

# export config
if quant_mode == 'calib':
  quantizer.export_quant_config()
if quant_mode == 'test':
  quantizer.export_xmodel()
