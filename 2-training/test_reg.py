import torch
from torch.utils.data import DataLoader
from torch import nn
import torchvision.transforms as transforms
import torch.optim as optim
from dataset import LaneDataset
from model_reg import Net

transform = transforms.Compose([
    transforms.ConvertImageDtype(torch.float),
    transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))])

test_dataset = LaneDataset('dataset_rgb/test.csv', transform=transform)
testloader = DataLoader(test_dataset, batch_size=500, num_workers=6)

net = Net()

net.load_state_dict(torch.load('state_dict_rgb.pth'))
net.eval()
device = 'cuda' if torch.cuda.is_available() else 'cpu'
print(f'Using {device} device')
net.to(device)

correct = 0.0
total = 0
# since we're not training, we don't need to calculate the gradients for our outputs
with torch.no_grad():
    for data in testloader:
        inputs, labels = data
        inputs = inputs.to(device)
        labels = labels.to(device)
        # calculate outputs by running images through the network
        outputs = net(inputs)
        total += labels.size(0)
        correct += (labels - outputs).abs().sum()
        print("Group ",(labels - outputs))

print('Tot angle err: {}'.format(correct))
print('Avg angle err: {}'.format(correct / total))