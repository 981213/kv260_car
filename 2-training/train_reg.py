#!/usr/bin/env python3
import torch
from torch.utils.data import DataLoader
from torch import nn
import torchvision.transforms as transforms
import torch.optim as optim
from dataset import LaneDataset
from model_reg import Net

transform = transforms.Compose([
    transforms.ConvertImageDtype(torch.float),
    transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))])

train_dataset = LaneDataset('dataset_rgb/labels.csv', transform=transform)
dataloader = DataLoader(train_dataset, batch_size=500, shuffle=True, num_workers=6)

device = 'cuda' if torch.cuda.is_available() else 'cpu'
print(f'Using {device} device')

net = Net()

#if torch.cuda.device_count() > 1:
#  print("Let's use", torch.cuda.device_count(), "GPUs!")
#  net = nn.DataParallel(net)
#net.load_state_dict(torch.load('state_dict2.pth'))
net.to(device)

criterion = nn.MSELoss()
optimizer = optim.Adam(net.parameters(), lr=0.0001)
scheduler = optim.lr_scheduler.ExponentialLR(optimizer, gamma=0.9)

for epoch in range(20):  # loop over the dataset multiple times

    running_loss = 0.0
    for i, data in enumerate(dataloader, 0):
        # get the inputs; data is a list of [inputs, labels]
        inputs, labels = data
        inputs = inputs.to(device)
        labels = labels.to(device)

        # zero the parameter gradients
        optimizer.zero_grad()

        # forward + backward + optimize
        outputs = net(inputs)
        loss = criterion(outputs, labels)
        loss.backward()
        optimizer.step()
        scheduler.step()

        # print statistics
        running_loss += loss.item()
        print('[%d, %d] loss: %.5f' % (epoch, i, running_loss))
        running_loss = 0.0
    torch.save(net.state_dict(), 'state_dict_rgb.pth')

print('Finished Training')
