SUMMARY = "RTL88x2BU driver"
LICENSE = "GPL-2.0"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/GPL-2.0;md5=801f80980d171dd6425610833a22dbe6"

inherit module

PR = "r0"
PV = "20210702"

SRC_URI = "git://github.com/morrownr/88x2bu-20210702;branch=main \
           file://0001-makefile.patch"
SRCREV = "f56d097a8b44043a13ac49e9a509a36738c0bf25"

S = "${WORKDIR}/git"

# The inherit of module.bbclass will automatically name module packages with
# "kernel-module-" prefix as required by the oe-core build environment.