#
# This file is the vai20-2uart recipe.
#

SUMMARY = "Simple vai20-2uart to use fpgamanager class"
SECTION = "PETALINUX/apps"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

inherit fpgamanager_custom
FPGA_MNGR_RECONFIG_ENABLE = "1"

SRC_URI = "file://shell.json \
           file://vai20-2uart.dtsi \
           file://vai20-2uart.xclbin \
           file://vai20-2uart.bit \
           "

RDEPENDS_${PN} += "ap1302-ar1335-single-firmware"

S = "${WORKDIR}"
