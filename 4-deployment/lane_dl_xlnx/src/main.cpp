#include <iostream>
#include <xir/graph/graph.hpp>
#include <vart/runner.hpp>
#include <vart/runner_ext.hpp>
#include <cmath>

#include <opencv2/opencv.hpp>

#include <ros/ros.h>
#include <ackermann_msgs/AckermannDriveStamped.h>

int main(int argc, char *argv[]) {
    ros::init(argc, argv, "lane_dl_xlnx");
    ros::NodeHandle n;
    ros::NodeHandle pn("~");

    std::string cmd_topic, cam_path, model_path;
    pn.param<std::string>("cmd_topic", cmd_topic, "ackermann_cmd");
    pn.param<std::string>("cam", cam_path, "/dev/video0");
    pn.param<std::string>("model", model_path, "net.xmodel");  
    ros::Publisher speed_pub = n.advertise<ackermann_msgs::AckermannDriveStamped>(cmd_topic, 1);

    auto graph = xir::Graph::deserialize(model_path);
    auto root = graph->get_root_subgraph();
    xir::Subgraph *subgraph = nullptr;
    for (auto c: root->children_topological_sort()) {
        CHECK(c->has_attr("device"));
        if (c->get_attr<std::string>("device") == "DPU") {
            subgraph = c;
            break;
        }
    }
    if (!subgraph) {
        ROS_ERROR_STREAM("no dpu subgraph in xmodel.");
        exit(-1);
    }
    auto attrs = xir::Attrs::create();
    std::unique_ptr<vart::RunnerExt> runner = vart::RunnerExt::create_runner(subgraph, attrs.get());

    // get input & output tensor buffers
    auto input_tensor_buffers = runner->get_inputs();
    auto output_tensor_buffers = runner->get_outputs();
    ROS_ERROR_COND(input_tensor_buffers.size() != 1u, "unsupported model");
    ROS_ERROR_COND(output_tensor_buffers.size() != 1u, "unsupported model");

    // get input_scale & output_scale
    auto input_tensor = input_tensor_buffers[0]->get_tensor();
    auto input_fixpos = input_tensor->template get_attr<int>("fix_point");
    //ROS_ERROR_COND(input_fixpos != 7, "hack doesn't work. Write it properly...");
    auto input_scale = std::exp2f(1.0f * (float)input_fixpos);

    auto output_tensor = output_tensor_buffers[0]->get_tensor();
    auto output_fixpos = output_tensor->template get_attr<int>("fix_point");
    auto output_scale = std::exp2f(-1.0f * (float) output_fixpos);

    auto n_batch = input_tensor->get_shape().at(0);
    ROS_ERROR_COND(n_batch != 1, "unsupported batch size");
    auto n_height = input_tensor->get_shape().at(1);
    auto n_width = input_tensor->get_shape().at(2);
    auto n_channel = input_tensor->get_shape().at(3);

    ROS_WARN_STREAM("Model H" << n_height << " W" << n_width);

    cv::VideoCapture cap;
    cap.open(cam_path.c_str());

    if (!cap.isOpened()) {
        ROS_ERROR_STREAM("cannot open " << cam_path);
        exit(-1);
    }

    cap.set(cv::CAP_PROP_FRAME_WIDTH, 640);
    cap.set(cv::CAP_PROP_FRAME_HEIGHT, 480);

    ackermann_msgs::AckermannDriveStamped pub_msg;
    double drive_speed;
    pn.param<double>("speed", drive_speed, 0.3);  
    pub_msg.drive.speed = drive_speed;

    while(ros::ok()) {
        // get a frame
        cv::Mat frame;
        if (!cap.read(frame)) {
            ROS_ERROR("failed to capture a frame.");
            break;
        }
        // crop & resize
        cv::Mat cropped_ref(frame, cv::Rect(0, 240, 640, 240));

        // preprocessing
        uint64_t data_in = 0u;
        size_t size_in = 0u;
        // set the input image and preprocessing
        std::tie(data_in, size_in) = input_tensor_buffers[0]->data(std::vector<int>{0, 0, 0, 0});
        CHECK_NE(size_in, 0u);
        auto *data_in_arr = (int8_t *) data_in;
        int parr = 0;
        for (auto row = 0; row < cropped_ref.rows; row++) {
            for (auto col = 0; col < cropped_ref.cols; col++) {
                auto pixel = cropped_ref.at<cv::Vec3b>(row, col);
                // CV image is BGR while model input is RGB. Flip it.
                for (int pix = 2; pix >= 0; pix--) {
                    auto val = pixel[pix];
                    float val_float = ((((float)val / 255.0f) - 0.5f) / 0.5f) * input_scale;
                    val_float = std::max(std::min(val_float, 127.0f), -128.0f);
                    data_in_arr[parr++] = (int8_t) val_float;
                }
            }
        }
        // sync data for input
        for (auto &input: input_tensor_buffers) {
            input->sync_for_write(0, input->get_tensor()->get_data_size() /
                                     input->get_tensor()->get_shape()[0]);
        }
        // start the dpu
        auto v = runner->execute_async(input_tensor_buffers, output_tensor_buffers);

        // do ROS work while DPU is running
        ros::spinOnce();

        auto status = runner->wait((int) v.first, -1);
        ROS_ERROR_COND(status != 0, "failed to run dpu");
        // sync data for output
        for (auto &output: output_tensor_buffers) {
            output->sync_for_read(0, output->get_tensor()->get_data_size() /
                                     output->get_tensor()->get_shape()[0]);
        }
        uint64_t data_o = 0u;
        size_t size_o = 0u;
        std::tie(data_o, size_o) = output_tensor_buffers[0]->data(std::vector<int>{0, 0});
        int8_t val = *(int8_t *) data_o;
        float val_fp = (float) val * output_scale;
        ROS_DEBUG_STREAM("Deg int :" << (int)val << " dec " << val_fp);
        pub_msg.drive.steering_angle = val_fp;
        speed_pub.publish(pub_msg);
        //cv::imshow("dst", cropped_ref);
        //cv::waitKey(1);
    }
    speed_pub.publish(ackermann_msgs::AckermannDriveStamped());
    ros::spinOnce();
    return 0;
}
